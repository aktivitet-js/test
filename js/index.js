import a from 'aktivitet';
import g from 'aktivitet/globals';
import s from 'aktivitet/selectors';

import c from '../config.js';

import '@aktivitet/assessment';
import '@aktivitet/page';

import dummy_store from '@aktivitet/dummy-store';
import scorm from '@aktivitet/scorm';
import template from '@aktivitet/template';
import Map from '@aktivitet/map';

const $ = g.$;
const $$ = g.$$;

const map = new Map( {
    'activity-tests': {
        'basic-test': {
            activity: 'page',
            options: {
                url: 'basic-test.html'
            }
        },
        'assessment': {
            activity: 'page',
            options: {
                url: 'assessment.html'
            },
            'page-test': {
                activity: 'page',
                options: {
                    url: 'page-test.html'
                }
            }
        }
    }
}, {
    event_listener: true
} );

a.map = map;

switch ( c.loader )
{
    case 'complete':
        // test custom loader
        g.loader = function complete_loader( $activity, options ) {
            return { flags: { is_completed: true } };
        };
    break;
    case 'scorm':
        scorm.init( '@aktivitet/test', window );
    break;
    default:
        dummy_store.init( '@aktivitet/test', c.loader );
    break;
}

template.templates.set( 'wrap', function ( $el, options ) {
    var c = $el.outerHTML;
    $el.outerHTML = '<div class="wrapped"><p>This <code>$el</code> is wrapped by <code>data-template</code></p>\n' + c + '</div>';
} );

a.template = template;

template.init();
a.init();

map.navigate('activity-tests/basic-test');

// top lvl completion logging
$('body').addEventListener( 'completed', function ( e ) {
    if ( e.target.matches( '[data-activity]' ) )
        console.log( 'c', e.target );
} );
g.$page_root.addEventListener( 'loaded', function ( e ) {
    template.init( s.PAGE_ROOT + ' [data-template]' );
} );


const debug = document.createElement( 'button' );
debug.dataset.debug = '';
debug.innerHTML = '&pi;';
$('body').append( debug );
g.$event_root.addEventListener( 'click', function ( e ) {
    if ( e.target.matches( 'button[data-debug]' ) )
        $('body').classList.toggle( 'debug' );
} );

export default a;